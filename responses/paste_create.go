package responses

import "gitlab.wikimedia.org/repos/sre/gonduit/entities"

// PasteCreateResponse represents the result of calling paste.create.
type PasteCreateResponse *entities.PasteItem

package responses

import "gitlab.wikimedia.org/repos/sre/gonduit/entities"

// RepositoryQueryResponse is the result of repository.query operations.
type RepositoryQueryResponse []*entities.Repository

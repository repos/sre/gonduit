package responses

import "gitlab.wikimedia.org/repos/sre/gonduit/entities"

// UserQueryResponse a response from calling user.query.
type UserQueryResponse []entities.User

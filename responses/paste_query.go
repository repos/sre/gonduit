package responses

import "gitlab.wikimedia.org/repos/sre/gonduit/entities"

// PasteQueryResponse represents the result of calling paste.query.
type PasteQueryResponse map[string]*entities.PasteItem

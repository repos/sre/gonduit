package responses

import "gitlab.wikimedia.org/repos/sre/gonduit/entities"

// ResolveRefsResponse represents the result of calling diffusion.resolverefs.
type ResolveRefsResponse map[string][]*entities.ResolvedRef

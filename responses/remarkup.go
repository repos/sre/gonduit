package responses

import "gitlab.wikimedia.org/repos/sre/gonduit/entities"

type RemarkupProcessResponse []*entities.RemarkupDocument

package responses

import "gitlab.wikimedia.org/repos/sre/gonduit/entities"

// DifferentialGetRawDiffResponse is the response from calling differential.getrawdiff.
type DifferentialGetRawDiffResponse *entities.DifferentialRawDiff

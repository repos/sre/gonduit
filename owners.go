package gonduit

import (
	"gitlab.wikimedia.org/repos/sre/gonduit/requests"
	"gitlab.wikimedia.org/repos/sre/gonduit/responses"
)

// OwnersSearch performs a call to owners.search
func (c *Conn) OwnersSearch(req requests.SearchRequest) (*responses.SearchResponse, error) {
	var res responses.SearchResponse
	if err := c.Call("owners.search", &req, &res); err != nil {
		return nil, err
	}
	return &res, nil
}

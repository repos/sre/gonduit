package gonduit

import (
	"gitlab.wikimedia.org/repos/sre/gonduit/requests"
	"gitlab.wikimedia.org/repos/sre/gonduit/responses"
)

// PhurlsSearch performs a call to phurls.search
func (c *Conn) PhurlsSearch(req requests.SearchRequest) (*responses.SearchResponse, error) {
	var res responses.SearchResponse
	if err := c.Call("phurls.search", &req, &res); err != nil {
		return nil, err
	}
	return &res, nil
}

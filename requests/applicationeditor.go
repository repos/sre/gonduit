package requests

import "gitlab.wikimedia.org/repos/sre/gonduit/entities"

type EditRequest struct {
	ObjectIdentifier entities.ObjectIdentifier `json:"objectIdentifier"`
	Transactions     []entities.Transaction    `json:"transactions"`
	Request
}
